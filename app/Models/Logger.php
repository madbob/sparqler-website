<?php

namespace App\Models;

class Logger extends \Psr\Log\AbstractLogger
{
	private $logs = [];

    public function log($level, $message, array $context = array())
    {
		if ($level == 'info') {
        	$this->logs[] = $message;
		}
    }

	public function getQuery()
	{
		return $this->logs[0] ?? '';
	}
}
