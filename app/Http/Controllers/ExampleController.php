<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use MadBob\Sparqler\Client;

use App\Models\Logger;

class ExampleController extends Controller
{
	private function initClient()
	{
		$client = new Client([
            'host' => 'https://query.wikidata.org/sparql',
            'omit_prefix' => true,
            'namespaces' => [
                'wd' => 'http://www.wikidata.org/entity/',
                'wdt' => 'http://www.wikidata.org/prop/direct/',
                'wdno' => 'http://www.wikidata.org/prop/novalue/',
                'wikibase' => 'http://wikiba.se/ontology#',
                'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
                'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
                'schema' => 'http://schema.org/',
                'p' => 'http://www.wikidata.org/prop/',
                'ps' => 'http://www.wikidata.org/prop/statement/',
                'pq' => 'http://www.wikidata.org/prop/qualifier/',
                'dct' => 'http://purl.org/dc/terms/',
                'ontolex' => 'http://www.w3.org/ns/lemon/ontolex#',
            ],
        ]);

		$logger = new Logger();
        $client->setLogger($logger);

        $httpclient = \EasyRdf\Http::getDefaultHttpClient();
        $httpclient->setConfig(['timeout' => 60]);

		return [$client, $logger];
	}

    public function run(Request $request)
	{
		$name = $request->input('name');
		$file = resource_path('samples/' . $name . '/code.txt');
		if (file_exists($file) == false) {
			abort(404);
		}

		$commons = "use MadBob\Sparqler\Terms\Iri; use MadBob\Sparqler\Terms\Raw; use MadBob\Sparqler\Terms\Variable; use MadBob\Sparqler\Terms\Aggregate; use MadBob\Sparqler\Terms\Optional; use MadBob\Sparqler\Terms\OwnSubject;";
		$code = file_get_contents($file);
		$code = $commons . "\n" . $code;

		list($client, $logger) = $this->initClient();

		try {
			eval($code);

			return view('commons.response', [
				'query' => $logger->getQuery(),
				'result' => print_r($query, true),
			]);
		}
		catch(\Exception $e) {
			return response()->json([
				'status' => 'ko',
				'error' => $e->getMessage(),
			]);
		}
	}
}
