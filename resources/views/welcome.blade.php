@extends('layout.main')

@section('contents')

<div>
    <section class="header-wrapper text-center position-relative">
        <div>
            <h1 class="display-1">SPARQLer</h1>
            <h2 class="display-5">Your "sparqling" ORM for PHP</h2>
        </div>

        <div class="position-absolute bottom-0 end-0">
            <small>Photo by <a rel="nofollow" href="https://unsplash.com/@gamzagaeguri?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Jaeyoon Jeong</a> on <a href="https://unsplash.com/s/photos/sparkling-wine?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></small>
        </div>
    </section>

    <section class="intro-wrapper">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>What is SPARQLer?</h3>
                    <p class="lead">
                        SPARQLer is a <a href="https://en.wikipedia.org/wiki/SPARQL">SPARQL</a>
                        <a href="https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping">Object-Relational Mapping</a>
                        for PHP, built on top of <a href="https://www.easyrdf.org/">EasyRDF</a> (the most popular PHP library for RDF handling).
                        In other words: a PHP library to access linked data sources in a object-oriented flavour, hiding the SPARQL query language
                        behind a set of convenient (and ofter more familiar) structures and functions.
                    </p>

                    <h3>Why SPARQLer?</h3>
                    <p class="lead">
                        Most developers are seasoned with the SQL query language and relational databases (like MariaDB or PosteGreSQL),
                        where information is organized in tables and columns. Most of the potentials of publicly available and collectively updated
                        linked "graphs" (like <a href="https://www.wikidata.org/">Wikidata</a> or <a href="https://www.dbpedia.org/">DBPedia</a>)
                        are still untapped due scarse adoption and the steep learning curve in understanding a different information model.<br>
                        <strong>SPARQLer</strong> provides a SQL-like fluent interface to such informations, and permit to a larger audience
                        to approach those tools. The <strong>SPARQLer</strong> API is largely inspired by the Laravel's native SQL ORM,
                        <a href="https://laravel.com/docs/eloquent">Eloquent</a>, which is already used by many PHP developers.<br>
                        Plus, even if you already know SPARQL, <strong>SPARQLer</strong> is a convenient interface to dinamically build your queries
                        and wrap data in the model of your application.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="usage-wrapper" id="usage-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @php
                    echo \Michelf\MarkdownExtra::defaultTransform(file_get_contents(resource_path('docs/install.md')));
                    echo \Michelf\MarkdownExtra::defaultTransform(file_get_contents(resource_path('docs/intro.md')));
                    echo \Michelf\MarkdownExtra::defaultTransform(file_get_contents(resource_path('docs/glossary.md')));
                    echo \Michelf\MarkdownExtra::defaultTransform(file_get_contents(resource_path('docs/client.md')));
                    echo \Michelf\MarkdownExtra::defaultTransform(file_get_contents(resource_path('docs/builder.md')));
                    echo \Michelf\MarkdownExtra::defaultTransform(file_get_contents(resource_path('docs/terms.md')));
                    echo \Michelf\MarkdownExtra::defaultTransform(file_get_contents(resource_path('docs/resources.md')));
                    echo \Michelf\MarkdownExtra::defaultTransform(file_get_contents(resource_path('docs/extras.md')));
                    @endphp
                </div>
            </div>
        </div>
    </section>

    <section class="contacts-wrapper">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>SPARQLer</h3>
                </div>
            </div>
            <div class="row">
                <div class="col links">
                    <div class="row">
                        <div class="col-1">
                            <p><i class="bi-envelope" aria-hidden="true"></i></p>
                        </div>
                        <div class="col-11">
                            <p><a href="mailto:info@madbob.org">info@madbob.org</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1">
                            <p><i class="bi-code-slash" aria-hidden="true"></i></p>
                        </div>
                        <div class="col-11">
                            <p><a href="https://gitlab.com/madbob/sparqler">gitlab.com/madbob/sparqler</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1">
                            <p><i class="bi-gear" aria-hidden="true"></i></p>
                        </div>
                        <div class="col-11">
                            <p>powered by <a href="http://madbob.org/"><img src="images/mad.png" alt="MAD"></a></p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <a href="https://www.paypal.com/donate/?hosted_button_id=GRKVVL3E5KZ4Q">
                        <img src="images/paypal.png" alt="PayPal Donation" border="0">
                    </a>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
