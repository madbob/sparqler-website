<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>SPARQLer - SPARQL ORM for PHP</title>
        <meta name="description" content="Your SPARQLing ORM!">

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@madbob">
        <meta name="twitter:title" content="SPARQLer">
        <meta name="twitter:description" content="SPARQL ORM for PHP">
        <meta name="twitter:creator" content="@madbob">
        <meta name="twitter:image" content="https://sparqler.madbob.org/images/fb.png">

        <meta property="og:site_name" content="SPARQLer" />
        <meta property="og:title" content="SPARQLer" />
        <meta property="og:url" content="https://sparqler.madbob.org/" />
        <meta property="og:image" content="https://sparqler.madbob.org/images/fb.png" />
        <meta property="og:type" content="website" />
        <meta property="og:locale" content="en_US" />

        <script type="application/ld+json">
            {
                "@context" : "http://schema.org",
                "@type" : "SoftwareApplication",
                "name" : "SPARQLer",
                "abstract": "SPARQL ORM for PHP",
                "author" : {
                    "@type" : "Person",
                    "name" : "Roberto Guido"
                },
                "applicationCategory" : "Object-relational mapping",
                "operatingSystem": "web based",
                "url" : "https://sparqler.madbob.org/",
                "downloadUrl" : "https://packagist.org/packages/madbob/sparqler",
                "keywords": "SPARQL, RDF, linked data, object-relational mapping, ORM, PHP",
                "license": "https://spdx.org/licenses/AGPL-3.0-or-later.html"
            }
        </script>
    </head>
    <body>
		@yield('contents')

		<!-- Matomo -->
		<script type="text/javascript">
			var _paq = window._paq || [];
			/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
			_paq.push(["setDoNotTrack", true]);
			_paq.push(["disableCookies"]);
			_paq.push(['trackPageView']);
			_paq.push(['enableLinkTracking']);
			(function() {
				var u="//stats.madbob.org/";
				_paq.push(['setTrackerUrl', u+'matomo.php']);
				_paq.push(['setSiteId', '20']);
				var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
				g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
			})();
		</script>
		<noscript><p><img src="//stats.madbob.org/matomo.php?idsite=20&amp;rec=1" style="border:0;" alt="" /></p></noscript>
		<!-- End Matomo Code -->
	</body>
</html>
