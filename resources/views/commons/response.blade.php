<div class="response">
	<hr>
	<p>
		Generated query:<br>
		{{ $query }}
	</p>
	<hr>
	<p>
		<details>
			<summary>Click to display <code>print_r($query)</code></summary>
			<pre class="bg-white text-black">{{ $result }}</pre>
		</details>
	</p>
</div>
