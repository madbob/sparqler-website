<div class="card mb-2">
	<div class="card-body">
		<div class="row">
			<div class="col-6">
				<pre class="h-100">{{ file_get_contents(resource_path('samples/' . $sample . '/sparql.txt')) }}</pre>
			</div>
			<div class="col-6">
				<pre class="h-100">{{ file_get_contents(resource_path('samples/' . $sample . '/code.txt')) }}</pre>
			</div>
		</div>

		<div class="row mt-2">
			<div class="col">
				<div class="d-grid gap-0">
					<a href="{{ route('example.run', ['name' => $sample]) }}" class="btn btn-primary">Run!</a>
				</div>
			</div>
		</div>
	</div>
</div>
