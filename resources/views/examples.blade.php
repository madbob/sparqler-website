@extends('layout.main')

@section('contents')

<style>
.response pre {
	height: 100vh;
	overflow-y: scroll;
}
</style>

<div class="container mt-3">
	<div class="row align-items-center">
		<div class="col-6">
			<p>
				In this page there are a few examples of SPARQL queries got from the <a href="https://query.wikidata.org/">Wikidata Query Service</a> and the relative implementation in <strong>SPARQLer</strong>.
			</p>
			<p>
				For each example, you can click the "Run!" button to execute the <strong>SPARQLer</strong> code and read both the automatically generated SPARQL query and the result set, to see what to expect. Please note that most of the examples may take many seconds to display a result.
			</p>
			<p>
				On execution, <code>$client</code> is always inited as stated aside.
			</p>
			<p>
				For the full documentation of <strong>SPARQLer</strong>, <a href="/">see here</a>.
			</p>
		</div>

		<div class="col-6">
			<pre>use MadBob\Sparqler\Client;
use MadBob\Sparqler\Resource;
use MadBob\Sparqler\Collection;
use MadBob\Sparqler\Container;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\Raw;
use MadBob\Sparqler\Terms\Variable;
use MadBob\Sparqler\Terms\Aggregate;
use MadBob\Sparqler\Terms\Optional;
use MadBob\Sparqler\Terms\OwnSubject;

$client = new Client([
  'host' => 'https://query.wikidata.org/sparql',
  'omit_prefix' => true,
  'namespaces' => [
    'wd' => 'http://www.wikidata.org/entity/',
    'wdt' => 'http://www.wikidata.org/prop/direct/',
    'wdno' => 'http://www.wikidata.org/prop/novalue/',
    'wikibase' => 'http://wikiba.se/ontology#',
    'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
    'schema' => 'http://schema.org/',
    'p' => 'http://www.wikidata.org/prop/',
    'ps' => 'http://www.wikidata.org/prop/statement/',
    'pq' => 'http://www.wikidata.org/prop/qualifier/',
    'dct' => 'http://purl.org/dc/terms/',
    'ontolex' => 'http://www.w3.org/ns/lemon/ontolex#',
  ],
]);

$httpclient = \EasyRdf\Http::getDefaultHttpClient();
$httpclient->setConfig(['timeout' => 60]);</pre>
		</div>
	</div>

	@include('commons.sample', ['sample' => 'cats'])
	@include('commons.sample', ['sample' => 'goats'])
	@include('commons.sample', ['sample' => 'horses'])
	@include('commons.sample', ['sample' => 'cats_picture'])
	@include('commons.sample', ['sample' => 'map_hospitals'])
	@include('commons.sample', ['sample' => 'map_hackerspaces'])
	@include('commons.sample', ['sample' => 'count_humans'])
	@include('commons.sample', ['sample' => 'no_children'])
	@include('commons.sample', ['sample' => 'born_newyork'])
	@include('commons.sample', ['sample' => 'with_wikispecies'])
	@include('commons.sample', ['sample' => 'authors_wikispecies'])
	@include('commons.sample', ['sample' => 'eyecolors'])
	@include('commons.sample', ['sample' => 'undefined_gender'])
	@include('commons.sample', ['sample' => 'wikipedia_urls'])
	@include('commons.sample', ['sample' => 'wikipedia_article_languages'])
	@include('commons.sample', ['sample' => 'with_property'])
	@include('commons.sample', ['sample' => 'items_wikipedia_articles'])
	@include('commons.sample', ['sample' => 'oresund'])
	@include('commons.sample', ['sample' => 'mayors_animals'])
	@include('commons.sample', ['sample' => 'mayors_dog'])
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>

$(document).ready(function() {
	$('.btn-primary').click(function(e) {
		e.preventDefault();
		var container = $(this).closest('.col');
		container.empty().append('<div class="text-center"><div class="spinner-border" role="status"></div></div>');

		$.ajax({
			url: $(this).attr('href'),
			method: 'GET',
			dataType: 'HTML',
			success: function(data) {
				container.empty().append(data);
			}
		});
	});
});

</script>

@endsection
