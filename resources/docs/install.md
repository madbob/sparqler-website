Install
=

To install **SPARQLer** just run

```
composer require madbob/sparqler
```

The full code, MIT licensed, is hosted <a href="https://gitlab.com/madbob/sparqler">on GitLab</a> (and, of course, open to contributions!).
