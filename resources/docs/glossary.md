Glossary
=

A few definitions useful to understand the **SPARQLer** API applied to the SPARQL model.

- **Client** is the object which provides the connection to the SPARQL endpoint. This includes the API to init a **Builder**
- **Builder** is the object used to compose and execute the actual query, as a concatenation of conditions and filters
- **Ontology** is the description of available **Predicates** within a given semantic context. Each ontology may refer to other ontologies, and the data into a SPARQL model usually include multiple ontologies. The name of an **Ontology** is an URL, more often rappresented with a prefix
- **Namespace** is the prefix used to short the name of **Predicates** included into a given **Ontology**. `http://xmlns.com/foaf/0.1/mbox` is the same of `foaf:mbox`
- **Term** is a single token into the query; there are many Term types, each providing a specific meaning and altering the behavior of the **Triple** in which appear and/or of the whole query
- **Triple** is a set of three **Term**: the first is the *subject* the second is *predicate* and the third is *object*. In the **SPARQLer** public API those are rappresented just as arrays, if a minor number of elements is provided (usually 2, sometime 1) the missing ones are filled implicitely
- **Subject** is a string, often an URL, that aggregates multiple **Triple**. In the SPARQL data model, all triples having the same "subject" belong to the same entity. In a **SPARQLer** query, this information is held into a **OwnSubject** term
- **Predicate** is the name of a single attribute in an entity: usually is an URL, or may be in "short" form using the prefix of the **Namespace** (e.g. `foaf:mbox`). In a **SPARQLer** query, those are wrapped into a **Iri** term (which may also rappresent the **Subject** of a specific entity)
- **Object** is the value for a given attribute. Often it is rappresented as a **Literal**
- **Result** is the object containing the product of a SELECT query: can be iterated and each element is an associative array in the form `predicate => value`
- **Literal** is an object usually used to enclose a value (a string, a number, a date...) and his own datatype. Leveraging [EasyRDF's TypeMapper](https://www.easyrdf.org/docs/api/EasyRdf/TypeMapper.html) it is possible to automatically convert those values in structured objects
- **Graph** is a collection of **Resource**, usually returned by a CONSTRUCT query
- **Resource** rappresents a single entity, having his own **Subject** and his own properties
