Resources and Graphs
=

The object returned by `doConstruct()` is a **Graph** (a collection of **Resource** objects): it directly extends [the EasyRDF Graph class](https://www.easyrdf.org/docs/api/EasyRdf/Graph.html), adding a few utilities.

First of all, the **SPARQLer's Graph** permits to access "top level" resources, those directly involved into the query. As a CONSTRUCT's graph includes also resources linked to those effectively queried, and those also are returned by native `resources()` method from EasyRDF, `masterResources()` method filters out only the resources which have been asked. **Graph** is also and iterable object, and when used into a `foreach` statement the master resources are iterated.

```
$graph = $this->client->doConstruct()
    ->where('dbo:type', new Iri('dbr:Capital_city'))
    ->where('dbo:timeZone', new Iri('dbr:Central_European_Time'))
    ->get();

/*
    This returns 7349: all resources involved into the query
    (the capital cities, their country, their region, their images...)
*/
count($graph->resources());

/*
    This returns 18: the actual capital cities in CET
*/
count($graph->masterResources());
```

Then, **Graph** has a `commit()` method used to push back into the SPARQL endpoint all properties inserted, deleted and modified from the child resources. This is useful to update once multiple resources, and bypass the fact that SPARQL do not provides a native way to perform an UPDATE query (like in SQL).

```
$graph = $client->doConstruct()->where('foaf:currentProject', new Iri('http://mydomain/Project/Bar'))->get();

foreach($graph as $resource) {
    // ... perform multiple set() or add() operations on $resource...
}

$graph->commit();
```
