Extras
=

**Builder** includes a few extra method, useful in particular situations.

`withWikiDataLabels()` appends to the query the formula to access the "wikibase:label" service in Wikidata. Can be chained to the query, before the final instruction for execution, with two parameters: an array of language identifiers (`['en']` is the default), and an array including **OwnSubject** (the default), any **Variable** appearing on the query, or some **Iri**: the elemens not explicitely included in the SELECTed predicates will be added anyway to the result set.
