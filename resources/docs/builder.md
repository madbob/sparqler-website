Builder
=

Into the **Builder** happen most of the definition of a SPARQL query, as you define here all of your WHERE conditions. Most of his functions return the same **Builder**, so to be chained.

Here a summary of the different options you have.

Example                                                                                   | SPARQL
------------------------------------------------------------------------------------------|---
&nbsp;|&nbsp;
`where('rdf:predicate', 'value')`                                                         | `?subject rdf:predicate 'value'`
| The most common condition: the predicate is applied to the implicit **OwnSubject** of the query. By default, the first parameter is wrapped within a **Iri** term, the second in a **Plain** (assumes it is a string)
&nbsp;|&nbsp;
`where('rdf:predicate', new Iri('a:subject'))`                                            | `?subject rdf:predicate a:subject`
| Passing **Term** objects as parameters, you can enforce their meaning and the way those will be appended into the query. This is true for every conditional function of the **Builder**
&nbsp;|&nbsp;
`where('rdf:predicate', function($query) { $query->where('rdf:other', 'value') })`        | `?subject rdf:predicate ?variable . ?variable rdf:other 'value'`
| The value of the condition can be a sub-query: a new random **Variable** will be used for further comparison an evaluations
&nbsp;|&nbsp;
`where(new Variable('foo'), 'rdf:predicate', 'value')`                                    | `?foo rdf:predicate 'value'`
| When three parameters are passed to `where()`, they become a complete **Triple**
&nbsp;|&nbsp;
`whereOptional('rdf:predicate', 'value')`                                                 | `OPTIONAL { ?subject rdf:predicate 'value' }`
| An OPTIONAL condition is to filter entities having a given predicate with a given value, or not that predicate at all. To optionally select a given predicate into a SELECT query it is more convenient to use the **Optional** term
&nbsp;|&nbsp;
`where('rdf:predicate', '!=', 'value')`                                                   | `?subject rdf:predicate ?variable . FILTER ( ?variable != 'value' )`
| Basic evaluation functions are built into the **where** function, which generates proper `FILTER` conditions. Supported operators: `< > <= >= !=`
&nbsp;|&nbsp;
`whereIn('rdf:predicate', ['value', 'value2'])`                                           | `?subject rdf:predicate ?value VALUES ?value { 'value' 'value2' }`
| Many different values can be matched at once
&nbsp;|&nbsp;
`whereReverse('rdf:predicate', new Iri('a:subject'))`                                     | `a:subject rdf:predicate ?subject`
| To reverse the operands of the **Triple**, and use the implicit **OwnSubject** as object instead of subject. Here, the second parameter is the new subject of the **Triple** (may be an explicit **Iri** or a **Variable** filtered somewhere else)
&nbsp;|&nbsp;
`whereReverse('rdf:predicate', function($query) { $query->where('rdf:other', 'value') })` | `a:subject rdf:predicate ?subject . a:subject rdf:other 'value'`
| Also reverted relations can be extended with sub-queries, in which the subject will be inherited from the parent one
&nbsp;|&nbsp;
`whereRaw('SPARQL expression')`                                                           | `SPARQL expression`
| An arbitrary expression can be appended to the query
&nbsp;|&nbsp;
`filter(function($query) { $query->where('rdf:predicate', 'value') })`                    | `FILTER { ?subject rdf:predicate 'value' }`
| Filters are used to refine the result set with given parameters. To be used for more complex evaluations than the basic ones built into the `where` function
&nbsp;|&nbsp;
`filterNotExists(function($query) { $query->where('rdf:predicate', 'value') })`           | `FILTER NOT EXISTS { ?subject rdf:predicate 'value' }`
| Reverts the filter, matching entities for which the given sub query produces no results
&nbsp;|&nbsp;
`minus(function($query) { $query->where('rdf:predicate', 'value') })`                     | `MINUS { ?subject rdf:predicate 'value' }`
| Part of the entities matching a query can be excluded from the final result is they match some other condition

Once you have appended all your conditions, you can finalize the **Builder** with one of these functions:

- `get()` is used for `doSelect()` and `doConstruct()` builders: the query is executed to the SPARQL endpoint defined for the parent **Client** and a result is returned. More exactly: `doSelect()` returns a **Result**, `doConstruct()` returns a **Graph**
- `run()` is for builders that do not have a result, those inited with `doInsert()` and `doDelete()`
- `count()` always performs a SELECT COUNT query (even when not inited with `doSelect()`) and returns the number of entities matching the conditions
- `queue()` can be used for multiple `doInsert()` and `doDelete()` queries, to be concatenated and execute once on the SPARQL endpoint

```
$client->doInsert([...])->where(...)->queue();
$client->doInsert([...])->where(...)->queue();
$client->doDelete([...])->where(...)->queue();
$client->runQueue();
```
