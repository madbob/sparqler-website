SPARQL 101
=

For reference and introduction, lets make a comparison among SQL and SPARQL.

An usual SQL query looks like:

<pre>
SELECT <span class="text-warning">column1, column2</span> FROM table WHERE <span class="text-info">column3 = 'something'</span>;
</pre>

while an equivalent SPARQL query looks like:

<pre>
SELECT ?foo ?bar WHERE <span class="text-warning">?item column1 ?foo . ?item column2 ?bar</span> . <span class="text-info">?item column3 'something'</span>;
</pre>

The basic abstraction of SQL is that the data is rappresented as rows into a table, with multiple columns, and each column contains a value and has his own name; the value of a given column can be extracted by matching the value of known other columns within the same row.

The basic abstraction of SPARQL is that you have a single table with only three columns ("subject", "predicate" and "object"), and rows with the same "subject" belongs to the same entity; using multiple combinations of *triples* subject/predicate/object, where each element can be a parameter (the tokens starting with `?`) having to eventually match in other triples where it appears, you can retrieve the required information.

**SPARQLer** permits to create SPARQL queries in a more "SQL-like" fashion, introducing a few implicit behavours (that can be overridden, if required, with certain functions and combinations of parameters).

<pre>
$client->doSelect(<span class="text-warning">['column1', 'column2']</span>)->where(<span class="text-info">'column3', 'something'</span>)->get();
</pre>

Where not otherwise explicited, the "subject" part of the SPARQL query is implicit: the selected attributes and all attributes used in the conditions refer to the same value.

For some example of actual SPARQL queries, in raw format and rewrote with **SPARQLer**, get a look to [the examples page](/examples).
