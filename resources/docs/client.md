Client
=

First of all, you need a **Client** to build and execute your SPARQL queries.

```
require_once "./vendor/autoload.php";
use MadBob\Sparqler\Client;
$client = new Client($config);
```

where `$config` is an associative array with the following keys:

- **host** (required): the SPARQL endpoint you want to query
- **graph** (optional): the name of the graph you want to query. Useful if you want to interact with your own linked data store
- **auth** (optional): an associative array with the authentication parameters you need to access the SPARQL endpoint. Required in particular if you want to interact with your own linked data store. If defined, it must contain the following keys:
  - **type**: one of the authentication method  [supported by Guzzle](https://docs.guzzlephp.org/en/stable/request-options.html#auth): basic, digest or ntml
  - **username**: your username
  - **password**: your password
- **namespaces** (optional): an associative array with the [namespaces](https://prefix.cc/) to be used in your queries. By default EasyRDF already defines the most common ones; if this parameter is defined, the new array overrides the default one
- **omit_prefix** (optional): if set to true, the final queries will not include PREFIXes at the beginning

The internal HTTP client used to perform requests to the SPARQL endpoint can be accessed and configured as desidered. By default it is an instance of [EasyRDF-on-Guzzle](https://packagist.org/packages/madbob/easyrdf-on-guzzle) to integrate a full-featured Guzzle client (using a cURL handler) with EasyRDF, so you can consult the [Guzzle documentation](https://docs.guzzlephp.org/en/stable/index.html) for more options.

```
$client = new Client($config);
$httpclient = \EasyRdf\Http::getDefaultHttpClient();
$httpclient->setConfig(['timeout' => 20]);
```

It is also possible to attach to the **Client** a [PSR-3 LoggingInterface](https://www.php-fig.org/psr/psr-3/), where all generated queries are saved, and a [PSR-16 CacheInterface](https://www.php-fig.org/psr/psr-16/), used only for inference functions.

```
$client = new Client($config);
$client->setLogger($your_LoggerInterface);
$client->setCache($your_CacheInterface);
```

A few sample inititialization for common use cases:

```
// Wikidata client
$client = new Client([
  'host' => 'https://query.wikidata.org/sparql',

  // For more namespaces used in Wikidata:
  // https://www.mediawiki.org/wiki/Wikibase/Indexing/RDF_Dump_Format#Full_list_of_prefixes
  'namespaces' => [
    'wd' => 'http://www.wikidata.org/entity/',
    'wdt' => 'http://www.wikidata.org/prop/direct/',
    'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
  ],
]);

// DBPedia client
$client = new Client([
  'host' => 'https://dbpedia.org/sparql',

  // For more namespaces used in DBPedia:
  // https://dpedia.org/sparql/?help=nsdecl
  'namespaces' => [
    'dbp' => 'http://dbpedia.org/property/',
    'dbr' => 'http://dbpedia.org/resource/',
    'dbo' => 'http://dbpedia.org/ontology/',
    'dct' => 'http://purl.org/dc/terms/',
    'dbc' => 'http://dbpedia.org/resource/Category:',
    'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
  ],
]);

// Your own Virtuoso server
$client = new Client([
  'host' => 'http://localhost:8890/sparql-auth',
  'graph' => 'urn:sparql:tests:insert:informative',
  'auth' => [
    'type' => 'digest',
    'username' => 'dba',
    'password' => 'your_password',
  ],
]);
```

From a **Client** instance, it is possible to obtain a **Builder** for each kind of query you want to perform.

doSelect and doSelectDistinct
-

`doSelect()` inits a SELECT query: the function accepts an array of items (by default plain strings are converted to **Prefixed** terms) that will be retrieved for each entity matching the conditions appended to the Builder. The query returns a **Result**.

```
$result = $client->doSelect([new OwnSubject(), 'dbp:name'])
	->where('dct:subject', new Iri('dbc:Capitals_in_Europe'))
	->get();
```

`doSelectDistinct()` acts in the same way, but inits a SELECT DISTINCT query.

doConstruct
-

`doConstruct()` returns a **Graph**, including multiple **Resource**, and permits an accurate selection of predicates to be fetched and added to the **Graph** itself.

```
$result = $client->doConstruct([['dbp:areaTotalSqMi'], ['dbp:website']])
	->where('dct:subject', new Iri('dbc:Capitals_in_Europe'))
	->get();
```

`doConstruct()` gets an optional array of **Triple** as parameters, describing the properties you want to fetch for each entity matching the conditions, but usually you may want to omit the subject (implicit, due the conditions appended to the **Builder**) and the object (which is automatically mapped into the query for each required predicate). If no parameters are passed, all the predicates of matching entities are fetched from the endpoint.

For convenience, **Client** has a short hand `find()` function which CONSTRUCTs a given subject.

```
$result = $client->find('dbr:Dublin');
```

doInsert and doDelete
-

`doInsert()` and `doDelete()` are used to insert and delete data into the graph. Both accept an array of **Triple** to specify what to insert or remove, while the conditions appended to the **Builder** define which entitiesa are the target of insert or remove operation.

```
$client->doInsert([
    ['foaf:knows', new Iri('http://mydomain/Person/Foo')],
])->where('foaf:currentProject', new Iri('http://mydomain/Project/Bar'))->run();
```

```
$client->doDelete([
    ['foaf:knows', new Iri('http://mydomain/Person/Foo')],
])->where('foaf:currentProject', new Iri('http://mydomain/Project/Bar'))->run();
```
