Terms
=

All of the tokens into each **Triple** must be enclosed within a wrapper **Term** class: if you just pass a string, the **Term** type is automatically assigned by his position into the **Triple** itself and his content.

Here a summary of each **Term** type, with some example.

Class        | Description
-------------|---
&nbsp;|&nbsp;
MadBob\Sparqler\Terms\Variable		| Any SPARQL variable (the tokens having a `?` before the name). If no parameters are passed, the name is randomly generated.
        							| This is usually used if you explicitely want a given value to be used in different parts of the query: init one or more **Variable** PHP variables and pass it as parameter to the different functions
&nbsp;|&nbsp;
MadBob\Sparqler\Terms\OwnSubject	| The subject of the query. Can be used as a **Variable** (actually: it is a **Variable**) and placed in different parts of the query.
									| Multiple **OwnSubject** distributed within the same query will have the same value (note: subqueries are not part of the parent queries, so a different **OwnSubject** will be assigned)
&nbsp;|&nbsp;
MadBob\Sparqler\Terms\Iri			| Wraps all predicate names and entity subjects.
									| The parameters passed to `doSelect()`, and the first parameter of `where()` functions (if only two are passed), are automatically wrapped within a **Iri**
&nbsp;|&nbsp;
MadBob\Sparqler\Terms\Plain			| Includes a generic string that will be enclosed between quotes in the final query.
									| Useful to explicitely enforce a string where another type of **Term** is expected
&nbsp;|&nbsp;
MadBob\Sparqler\Terms\Raw			| Includes a generic string that will be appended as-is (with no quotes or escapes) in the final query.
									| Useful to enforce specific parts of the query and special syntax non handled by **SPARQLer**
&nbsp;|&nbsp;
MadBob\Sparqler\Terms\Optional		| Used to wrap a **Iri** (default, if a plain string is passed) or a **Variable** to be optionally added into the result set. Mostly used in the parameters list of `doSelect()`
&nbsp;|&nbsp;
MadBob\Sparqler\Terms\Aggregate		| A SPARQL function applied to some value. Parameters are: the name of the function, the parameter(s) to that function (note: plain strings will be handled as **Iri**), and optionally an alternative name to hold the final result of the function.
									| E.g. `Aggregate('COUNT', 'rdf:predicate', 'counter')` becomes `COUNT(?xyz) as ?counter ... WHERE ... ?subject rdf:predicate ?xyz`
