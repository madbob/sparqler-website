SELECT ?human ?humanLabel
WHERE {
  ?human wdt:P31 wd:Q5 .
  ?human rdf:type wdno:P40 .
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en" }
}
