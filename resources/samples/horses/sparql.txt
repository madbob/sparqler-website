SELECT DISTINCT ?horse ?horseLabel ?mother ?father (year(?birthdate) as ?birthyear) (year(?deathdate) as ?deathyear) ?genderLabel
WHERE {
  ?horse wdt:P31/wdt:P279* wd:Q726 .
  OPTIONAL{?horse wdt:P25 ?mother .}
  OPTIONAL{?horse wdt:P22 ?father .}
  OPTIONAL{?horse wdt:P569 ?birthdate .}
  OPTIONAL{?horse wdt:P570 ?deathdate .}
  OPTIONAL{?horse wdt:P21 ?gender .}

  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "[AUTO_LANGUAGE],fr,ar,be,bg,bn,ca,cs,da,de,el,en,es,et,fa,fi,he,hi,hu,hy,id,it,ja,jv,ko,nb,nl,eo,pa,pl,pt,ro,ru,sh,sk,sr,sv,sw,te,th,tr,uk,yue,vec,vi,zh"
  }
}
ORDER BY ?horse
