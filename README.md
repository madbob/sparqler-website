SPARQLer Website
================

See https://sparqler.madbob.org/

Credits
=======

- background photo: by Jaeyoon Jeong on Unsplash (Public Domain)
  https://unsplash.com/photos/3I02CT894Qg

- icon: Merlin2525 on OpenClipArt (Public Domain)
  https://openclipart.org/detail/173632/champagne-glass-remix
